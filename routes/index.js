var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Control Online', name: '' });
});

router.get('/game/:name', function(req, res) {
  res.render('game', {name: req.params.name });
});

module.exports = router;
