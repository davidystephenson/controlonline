var Announcement = require('./announcement.js');
var Asteroid = require('./asteroid.js');
var Roamer = require('./roamer.js');
var Wall = require('./wall.js');

module.exports = function(name) {
  var game = {};

  game.actors = [];

  game.actors.push(Roamer([700, 400]));

  game.size = [10000, 16180];

  // Left
  game.actors.push(Wall([-500, game.size[1] / 2], 1000, game.size[1]));

  // Right
  game.actors.push(Wall([game.size[0] + 500, game.size[1] / 2], 1000, game.size[1]));

  // Top
  game.actors.push(Wall([game.size[0] / 2, -500], game.size[0], 1000));

  // Bottom
  game.actors.push(Wall([game.size[0] / 2, game.size[1] + 500], game.size[0], 1000));

  var gameArea = game.size[0] * game.size[1];

  var portion = gameArea / 5;

  console.log('portion test:', portion);
  var area = 0;

  var runif = function(minimum, maximum) { return minimum + Math.random() * (maximum - minimum); };

  while (area < portion) {
    var actor;
    var x;
    var y;

    var size = runif(1000, portion / 50);
    console.log('\nsize test:', size);
    area += size;

    var actorChoice = Math.random();
    console.log('actorChoice test:', actorChoice);
    if (actorChoice > 0.5) {
      var width = runif(100, size / 100);
      var height = size / width;

      x = runif(100 + width / 2, game.size[0] - 100 - width / 2);
      console.log('x test:', x);

      y = runif(100 + height / 2, game.size[1] - 100 - width / 2);
      console.log('y test:', y);

      actor = Wall([x, y], width, height);
    } else {
      var radius = Math.sqrt(size / Math.PI);

      x = runif(100 + radius, game.size[0] - radius - 100);
      y = runif(100 + radius, game.size[1] - radius - 100);
      actor = Asteroid([x, y], radius);
    }

    game.actors.push(actor);     
  }

  console.log('actors test:', game.actors.length);

  game.clients = {};

  game.maximum = 10;

  game.name = name;

  game.announcements = [Announcement('Welcome to <strong>' + game.name + '</strong>', false)];


  return game;
};
