module.exports = function(home) {
  var physics = {};

  physics.circleCircle = function(collision) {
    if (collision.distance < collision.shape1.radius + collision.shape2.radius) {
      collision.normal = home.geometry.unit(
        home.geometry.subtract(collision.position1, collision.position2)
      );
    }
  };

  physics.rectangleRectangle = function(collision) {
    widths = collision.shape1.width / 2 + collision.shape2.width / 2;
    heights = collision.shape1.height / 2 + collision.shape2.height / 2;

    overlapX = Math.max(0, widths - collision.distanceX);
    overlapY = Math.max(0, heights - collision.distanceY);

    if ((overlapX > 0) && (overlapY > 0)) {
      if (overlapX < overlapY) {
        collision.normal = home.geometry.unit([collision.position1[0] - collision.position2[0], 0]);
      } else {
        collision.normal = home.geometry.unit([0, collision.position1[1] - collision.position2[1]]);
      }
    }
  };

  physics.circleRectangle = function(collision) {
    var width = collision.shape2.width / 2;
    var height = collision.shape2.height / 2;
    var widths = collision.shape1.radius + width;
    var heights = collision.shape1.radius + height;

    var overlapX = Math.max(0, widths - collision.distanceX);
    var overlapY = Math.max(0, heights - collision.distanceY);

    if ((overlapX > 0) && (overlapY > 0)) {
      if (
        (
          collision.position2[0] - width < collision.position1[0] &&
          collision.position1[0] < collision.position2[0] + width
        ) ||
        (
          collision.position2[1] - height < collision.position1[1] &&
          collision.position1[1] < collision.position2[1] + height
        )
      ) {
        if (overlapX < overlapY) {
          collision.normal = home.geometry.unit([collision.position1[0] - collision.position2[0], 0]);
        } else {
          collision.normal = home.geometry.unit([0, collision.position1[1] - collision.position2[1]]);
        }
      } else {
        var topLeft = home.geometry.add(collision.position2, [-width, -height]);
        var topRight = home.geometry.add(collision.position2, [width, -height]);
        var bottomLeft = home.geometry.add(collision.position2, [-width, height]);
        var bottomRight = home.geometry.add(collision.position2, [width, height]);

        var topLeftDistance = home.geometry.distance(collision.position1, topLeft);
        var topRightDistance = home.geometry.distance(collision.position1, topRight);
        var bottomLeftDistance = home.geometry.distance(collision.position1, bottomLeft);
        var bottomRightDistance = home.geometry.distance(collision.position1, bottomRight);

        var closest = Math.min(topLeftDistance, topRightDistance, bottomLeftDistance, bottomRightDistance);

        if (closest < collision.shape1.radius) {
          collision.normal = home.geometry.unit(
            home.geometry.subtract(collision.position1, collision.position2)
          );
        }
      }
    }
  };

  physics.rectangleCircle = function(collision) {
     var width = collision.shape1.width / 2;
     var height = collision.shape1.height / 2;
     var widths = collision.shape2.radius + width;
     var heights = collision.shape2.radius + height;

     var overlapX = Math.max(0, widths - collision.distanceX);
     var overlapY = Math.max(0, heights - collision.distanceY);

    if ((overlapX > 0) && (overlapY > 0)) {
      if (
        (
          collision.position1[0] - width < collision.position2[0] &&
          collision.position2[0] < collision.position1[0] + width
        ) ||
        (
          collision.position1[1] - height < collision.position2[1] &&
          collision.position2[1] < collision.position1[1] + height
        )
      ) {
        if (overlapX < overlapY) {
          collision.normal = home.geometry.unit([collision.position1[0] - collision.position2[0], 0]);
        } else {
          collision.normal = home.geometry.unit([0, collision.position1[1] - collision.position2[1]]);
        }
      } else {
        var topLeft = home.geometry.add(collision.position1, [-width, -height]);
        var topRight = home.geometry.add(collision.position1, [width, -height]);
        var bottomLeft = home.geometry.add(collision.position1, [-width, height]);
        var bottomRight = home.geometry.add(collision.position1, [width, height]);

        var topLeftDistance = home.geometry.distance(collision.position2, topLeft);
        var topRightDistance = home.geometry.distance(collision.position2, topRight);
        var bottomLeftDistance = home.geometry.distance(collision.position2, bottomLeft);
        var bottomRightDistance = home.geometry.distance(collision.position2, bottomRight);

        var closest = Math.min(topLeftDistance, topRightDistance, bottomLeftDistance, bottomRightDistance);

        if (closest < collision.shape2.radius) {
          collision.normal = home.geometry.unit(
            home.geometry.subtract(collision.position1, collision.position2)
          );
        }
      }
    }
  };

  return physics;
};
