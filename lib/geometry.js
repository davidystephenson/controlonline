module.exports = function(home) {
  var geometry = {};

  geometry.add = function(x, y) { return [x[0] + y[0], x[1] + y[1]]; };

  geometry.direction = function(x, y) { return geometry.unit(geometry.subtract(y, x)); };

  geometry.distance = function(x, y) { return geometry.magnitude(geometry.add(x, geometry.negate(y))); };

  geometry.divide = function(x, y) { 
    if (Array.isArray(x) && Array.isArray(y)) return [x[0] / y[0], x[1] / y[1]];
    else if (Array.isArray(x)) return [x[0] / y, x[1] / y];
    else return [x / y[0], x / y[1]];
  };

  geometry.magnitude = function(x) { return Math.sqrt(x[0] * x[0] + x[1] * x[1]); };

  geometry.multiply = function(x, y) {
    if (Array.isArray(x) && Array.isArray(y)) return [x[0] * y[0], x[1] * y[1]];
    else if (Array.isArray(x)) return [x[0] * y, x[1] * y];
    else return [x * y[0], x * y[1]];
  };

  geometry.negate = function(x) { return [-x[0], -x[1]]; };

  geometry.subtract = function(x, y) { return geometry.add(x, geometry.negate(y)); };

  geometry.unit = function(vector) {
    var magnitude = geometry.magnitude(vector);
    if (magnitude > 0) return geometry.divide(vector, geometry.magnitude(vector));
    else return [0, 0];
  };

  return geometry;
};
