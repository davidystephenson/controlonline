var Announcement = require('./announcement.js');
var Collision = require('./collision.js');
var Game = require('./game.js');

module.exports = function(home) {
  var director = {};

  director.move = function(game, actor) {
    if (!(actor.fixed)) {
      var collision = director.separate(game, actor);
      if (collision) {
          var ratio1 = collision.actor2.weight / (actor.weight + collision.actor2.weight);
          var ratio2 = actor.weight / (collision.actor2.weight + actor.weight);

          actor.position = home.geometry.add(
            actor.position, home.geometry.multiply(
              collision.normal, 2 * ratio1
            )
          );

          collision.actor2.position = home.geometry.add(
            collision.actor2.position, home.geometry.multiply(
              home.geometry.negate(collision.normal),
              2 * ratio2
            )
          );
      } else {
        actor.position = home.geometry.add(actor.position, actor.velocity);
      }
    }
  };

  director.navigate = function(actor) {
    actor.velocity = [0, 0];
    console.log('waypoints test:', actor.mind.waypoints);

    if (home.geometry.magnitude(actor.mind.direction) > 0) {
      actor.velocity = home.geometry.multiply(actor.mind.direction, actor.speed);
    } else {
      if (actor.mind.waypoints.length > 0) {
        var destination = actor.mind.waypoints[actor.mind.mark];
        if (home.geometry.distance(actor.position, destination) > actor.speed) {
          actor.velocity = home.geometry.multiply(
            home.geometry.direction(actor.position, destination), actor.speed
          );
        } else {
          if (actor.mind.waypoints.length > actor.mind.mark + 1) actor.mind.mark ++;
          else {
            actor.mind.destination = false;
            actor.mind.waypoints = [];
            actor.mind.mark = 0;
          }
        }
      }
    }
    console.log('velocity test:', actor.velocity);
  };

  director.separate = function(game, actor) {
    for (var key in game.actors) {
      var foreignActor = game.actors[key];

      if (actor.position == foreignActor.position) return false;

      for (var shapeIndex in actor.shapes) for (var foreignShapeIndex in foreignActor.shapes) {
        var shape = actor.shapes[shapeIndex];
        var foreignShape = foreignActor.shapes[foreignShapeIndex];

        var collision = Collision(actor, foreignActor, shape, foreignShape);

        if (shape.type == 'circle' && foreignShape.type == 'circle') home.physics.circleCircle(collision);
        else if (shape.type == 'rectangle' && foreignShape.type == 'rectangle') {
          home.physics.rectangleRectangle(collision);
        } else if (shape.type == 'circle' && foreignShape.type == 'rectangle') {
          home.physics.circleRectangle(collision);
        } else if (shape.type == 'rectangle' && foreignShape.type == 'circle') {
          home.physics.rectangleCircle(collision);
        }

        if (collision.normal) return collision;
      }
    }
    return false;
  };

  director.weight = function(actor) {
    for (var shape in actor.shapes) actor.weight += actor.shapes[shape].weight;
  };

  return director;
};
