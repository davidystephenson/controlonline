var Geometry = require('./geometry.js');

module.exports = function(actor1, actor2, shape1, shape2) {
  var geometry = Geometry();

  var collision = {};

  collision.actor1 = actor1;

  collision.actor2 = actor2;

  collision.future1 = geometry.add(collision.actor1.position, collision.actor1.velocity);

  collision.future2 = geometry.add(collision.actor2.position, collision.actor2.velocity);

  collision.normal = false;

  collision.shape1 = shape1;

  collision.shape2 = shape2;
  
  collision.position1 = geometry.add(collision.future1, collision.shape1.position);

  collision.position2 = geometry.add(collision.future2, collision.shape2.position);

  collision.distanceX = Math.abs(collision.position1[0] - collision.position2[0]);

  collision.distanceY = Math.abs(collision.position1[1] - collision.position2[1]);

  collision.distance = geometry.distance(collision.position1, collision.position2);

  return collision; 
};
