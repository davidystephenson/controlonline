module.exports = function() {

  var mind = {};

  mind.destination = false;
  
  mind.direction = [0, 0];

  mind.mark = 0;

  mind.waypoints = [];

  return mind;
};
