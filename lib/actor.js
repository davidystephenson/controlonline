var Mind = require('./mind.js');

module.exports = function(position, type) {
  var actor = {};

  actor.instability = [0, 0];

  actor.client = false;

  actor.fixed = false;

  actor.mind = Mind();

  actor.position = position;

  actor.speed = 5;

  actor.type = type;

  actor.velocity = [0, 0];

  actor.weight = false;

  actor.shapes = [];

  return actor;
};
