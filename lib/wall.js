var Actor = require('./actor.js');
var Rectangle = require('./rectangle.js');

module.exports = function(position, width, height) {
  var wall = Actor(position, 'wall');

  wall.destination = false;

  wall.fixed = true;

  wall.shapes = [Rectangle([0, 0], width, height)];

  return wall;
};
