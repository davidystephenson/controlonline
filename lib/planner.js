module.exports = function(home) {
  var planner = {};

  planner.control = function(client) {
   if (client.actor) {
      client.actor.mind.destination = client.control.destination;
      client.actor.mind.direction = [0, 0];
      if (client.control.up) client.actor.mind.direction[1] += -1;
      if (client.control.down) client.actor.mind.direction[1] += 1;
      if (client.control.left) client.actor.mind.direction[0] += -1;
      if (client.control.right) client.actor.mind.direction[0] += 1;
      client.actor.mind.direction = home.geometry.unit(client.actor.mind.direction);
    }
  };

  planner.navigate = function(actor) {
    if (actor.mind.waypoints.length === 0) {
      console.log('navigating test');
      actor.mind.waypoints = [actor.position, actor.mind.destination];
      actor.mind.mark = 1;
    }
  };

  planner.think = function(actor) {
    if (actor.type == 'roamer') planner.roam(actor);

    if (actor.mind.destination) planner.navigate(actor);
  };

  planner.roam = function(actor){
    if (actor.destination) {
      var distance = home.geometry.distance(actor.position, actor.mind.destination);
      var fear = 1 / (1 + Math.exp(distance * 0.02));
      if (Math.random() < fear) {
        actor.instability = [Math.random() * 1000, Math.random() * 618];
      }

      if (Math.random() < 0.01) {
        if (actor.instability[0] > actor.position[0]) {
          actor.instability[0] = 0.9 * actor.instability[0] + 0.1 * 1000;
        } else actor.instability[0] = 0.9 * actor.instability[0];
      }

      if (Math.random() < 0.01) {
        if (actor.instability[1] > actor.position[1]) {
          actor.instability[1] = 0.9 * actor.instability[1] + 0.1 * 618;
        } else actor.instability[1] = 0.9 * actor.instability[1];
      }

      var desX = ((0.97) * actor.mind.destination[0]) + ((0.03) * actor.instability[0]);
      var desY = ((0.97) * actor.mind.destination[1]) + ((0.03) * actor.instability[1]);
      actor.mind.destination = [desX, desY];
    } else {
      actor.mind.destination = [Math.random() * 1000, Math.random() * 618];
    }
  };

  return planner;
};
