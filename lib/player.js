var Actor = require('./actor.js');
var Circle = require('./circle.js');
var Director = require('./director.js');

module.exports = function(position, client) {
  var player = Actor(position, 'player');

  var director = Director();

  player.client = client;

  player.shapes = [Circle([0, 0], 15)];

  player.speed = 30;

  return player;
};
