var Actor = require('./actor.js');
var Circle = require('./circle.js');

module.exports = function(position) {
  var roamer = Actor(position, 'roamer');

  roamer.speed = (Math.random() * 2) + 1;

  roamer.shapes = [Circle([0, 0], 10)];

  return roamer;
};
