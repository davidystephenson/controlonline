var Camera = require('./camera.js');
var Control = require('./control.js');

module.exports = function(game, socketID) {
  client = {};

  client.control = Control();

  client.actor = false;

  client.game = game;

  client.name = false;

  client.requestedGame = false;

  client.requestedName = false;

  client.socketID = socketID;

  client.type = 'spectator';

  return client;
};
