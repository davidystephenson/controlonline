var Actor = require('./actor.js');
var Circle = require('./circle.js');

module.exports = function(position, radius) {
  var asteroid = Actor(position, 'asteroid');

  asteroid.destination = false;

  asteroid.shapes = [Circle([0, 0], radius)];

  return asteroid;
};
