var Shape = require('./shape.js');

module.exports = function(position, radius) {
  var circle = Shape(position);

  circle.radius = radius;

  circle.type = 'circle';

  circle.weight = radius * radius * Math.PI;

  return circle;
};
