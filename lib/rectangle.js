var Shape = require('./shape.js');

module.exports = function(position, width, height) {
  var rectangle = Shape(position);

  rectangle.height = height;

  rectangle.type = 'rectangle';

  rectangle.weight = width * height;

  rectangle.width = width;
  
  return rectangle;
};
