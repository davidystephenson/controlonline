var Player = require('./player.js');
var Announcement = require('./announcement.js');
var Game = require('./game.js');
var _ = require('underscore');

module.exports = function(home) {
  var registrar = {};

  registrar.isAlphaNumeric = function(text) {
    if (/^[A-Za-z0-9]+$/.test(text)) return true;
    else return false;
  };

  registrar.home = home;

  registrar.check = function(text, type, set, audience) {
    var error = false;
    if (text) {
      if (registrar.isAlphaNumeric(text)) {
        if (_.contains(set, text)) {
          error = home.buff(text) + ' is currently being used by another ' + type + '.';
        } else {
          return true;
        }
      } else {
        error = 'That ' + type + ' contains illegal characters or spaces.';
      }
    } else {
      error = 'That ' + type + ' is empty.';
    }

    audience.announcements.push(Announcement(error, client));
    return false;
  };

  registrar.requestGame = function(client) {
    if (client.control.game) {
      if (client.requestedGame != client.control.game) {
        registrar.createGame(client.control.game, client);
      }
      client.requestedGame = client.control.game;
    }
  };

  registrar.createGame = function(name, client) {
    if (registrar.check(name, 'game', Object.keys(home.games)), home) home.games[name] = Game(name);
    else return false;
  };

  registrar.join = function(client) {
    if (client.control.name) {
      if (client.requestedName != client.control.name) {
        var names = [];
        for (var peer in home.games[client.game].clients) {
          names.push(home.games[client.game].clients[peer].name);
        }

        if (registrar.check(client.control.name, 'player', names, home.games[client.game])) {
          client.name = client.control.name;
          client.type = 'player';
          var player = Player([500, 309], client.socketID);
          client.actor = player;
          home.games[client.game].actors.push(player);

          home.games[client.game].announcements.push(
            Announcement('You have joined as ' + home.buff(client.name) + '.', client)
          );
        }
      }
      client.requestedName = client.control.name;
    }
  };

  registrar.validate = function(client) {
    if (!(client.socketID in home.io.sockets.connected)) {
      if (client.game) {
        var announcement = Announcement(home.buff(client.name) + ' has left.', false);
        home.games[client.game].announcements.push(announcement);
        delete home.games[client.game].clients[client.socketID];
      } else {
        delete home.clients[client.socketID];
      }

      if (client.actor) {
        home.games[client.game].actors = _.without(home.games[client.game].actors, client.actor);
      }
    }
  };

  return registrar;
};
