var Director = require('./director.js');
var Geometry = require('./geometry.js');
var Physics = require('./physics.js');
var Planner = require('./planner.js');
var Registrar = require('./registrar.js');

module.exports = function(io) {
  var home = {};

  home.announcements = [];

  home.buff = function(text) { return '<strong>' + text + '</strong>'; };

  home.clients = {};

  home.director = Director(home);

  home.games = {};

  home.geometry = Geometry(home);

  home.io = io;

  home.physics = Physics(home);

  home.planner = Planner(home);

  home.print = function(obj) { return JSON.stringify(obj, null, 4); };

  home.randomElement = function(array) {
    return array[Math.floor(Math.random() * array.length)];
  };

  home.registrar = Registrar(home);

  home.randomKey = function(set) {
    return Object.keys(set)[Math.floor(Math.random() * Object.keys(set).length)];
  };

  home.update = function() {
    var absents = {};

    for (var client in home.clients) {
      home.registrar.requestGame(home.clients[client]);

      home.registrar.validate(home.clients[client]);
    }

    for (var game in home.games) {
      for (client in home.games[game].clients) {
        home.planner.control(home.games[game].clients[client]);
        

        home.registrar.join(home.games[game].clients[client]);

        home.registrar.validate(home.games[game].clients[client]);
      }

      for (var key in home.games[game].actors) {
        var actor = home.games[game].actors[key];

        if (!(actor.weight)) home.director.weight(actor);

        home.planner.think(actor);

        home.director.navigate(actor);

        home.director.move(home.games[game], actor);
      }
    }
  };

  return home;
};
