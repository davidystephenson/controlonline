var Home = require('../lib/home.js');
var Client = require('../lib/client.js');

module.exports = function(io) {
  var home = Home(io);

  var serverUpdate = function() {
    var lobby = {};
    lobby.games = home.games;
    lobby.announcements = home.announcements;
    io.to('index').emit('lobby', lobby);

    home.update();
  };

  var clientUpdate = function() {
    for (var game in home.games) io.to(home.games[game].name).emit('game', home.games[game]);
  };

  io.on('connection', function(socket) {
    socket.game = false;

    socket.on('index', function() {
      if (socket.game) socket.leave(socket.game);
      socket.game = false;
      socket.join('index');

      home.clients[socket.id] = Client(false, socket.id);
    });
    
    socket.on('control', function(control) {
      var client = false;
      if (socket.game) client = home.games[socket.game].clients[socket.id];
      else if (socket.id in home.clients) client = home.clients[socket.id];

      if (client) client.control = control;
    });

    socket.on('game', function(name) {
      socket.leave('index');
      if (socket.game) socket.leave(socket.game);

      if (!(name in home.games)) if (home.registrar.createGame(name)) return false; 

      socket.join(name);
      socket.game = name;

      var client = Client(socket.game, socket.id);
      home.games[socket.game].clients[socket.id] = client;

      socket.emit('client', client);
    });
  });

  setInterval(serverUpdate, 30);
  setInterval(clientUpdate, 30);
};
