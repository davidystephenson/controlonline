var control = {
  destination: false,
  game: false,
  name: false,
};

var lobby = {};

var socket = io();

var tick = function() {
  $('#games').empty();
  for (var key in lobby.games) {
    var game = lobby.games[key];

    $('#games').append('<a href="/game/' + game.name + '"class="list-group-item">' + game.name + '</a>');
  }

  $('#announcements').empty();
  for (key in lobby.annoucements) {
    var announcement = lobby.announcements[key];

    $('#announcements').prepend('<a class="list-group-item active">' + announcement.text + '</a>');
  }

  socket.emit('control', control);
};

socket.emit('index');

// Input
socket.on('lobby', function(message) { lobby = message; });

// Output
$('#create').click(function() { control.game = $('#game').val(); });

setInterval(tick, 500);
