var toggle = function(key, obj, property) {
  $(document).bind('keydown', key, function() { obj[property] = true; });
  $(document).bind('keyup', key, function() { obj[property] = false; });
};

// Camera
toggle('shift+w', camera, 'up');
$(document).bind('keydown', 'shift+w', function() { camera.track = false; });

toggle('shift+a', camera, 'left');
$(document).bind('keydown', 'shift+a', function() { camera.track = false; });

toggle('shift+s', camera, 'down');
$(document).bind('keydown', 'shift+s', function() { camera.track = false; });

toggle('shift+d', camera, 'right');
$(document).bind('keydown', 'shift+d', function() { camera.track = false; });

$(document).bind('keydown', '`', function() { camera.track = !camera.track; });

// Player
toggle('w', control, 'up');
$(document).bind('keydown', 'w', function() { control.destination = false; });

toggle('a', control, 'left');
$(document).bind('keydown', 'a', function() { control.destination = false; });

toggle('s', control, 'down');
$(document).bind('keydown', 's', function() { control.destination = false; });

toggle('d', control, 'right');
$(document).bind('keydown', 'd', function() { control.destination = false; });

