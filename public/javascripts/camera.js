var Camera = function() {
  var camera = {};
 
  camera.down = false;
 
  camera.left = false;

  camera.move = function(x) {
    camera.position[0] += x[0];
    camera.position[1] += x[1];
  };

  camera.position = [500, 309];

  camera.right = false;

  camera.scale = 1;

  camera.speed = 5;

  camera.track = true;

  camera.up = false;

  camera.velocity = [0, 0];

  return camera;
};
