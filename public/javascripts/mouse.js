canvas.onmousedown = function (e) {
  var x;
  var y;
  if (e.pageX || e.pageY) {
    x = e.pageX;
    y = e.pageY;
  } else {
    x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
	  y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  }

  // Convert to coordinates relative to the canvas
  var destination = [x - canvas.offsetLeft, y - canvas.offsetTop];

  if (client.type == 'player') {
    destination = subtract(destination, [500, 309]);
    destination = multiply(destination, camera.scale);
    destination = add(destination, camera.position);
    control.destination = destination;
  }
};
