var add = function(x, y) { return [x[0] + y[0], x[1] + y[1]]; };

var divide = function(x, y) { 
  if (Array.isArray(x) && Array.isArray(y)) return [x[0] / y[0], x[1] / y[1]];
  else if (Array.isArray(x)) return [x[0] / y, x[1] / y];
  else return [x / y[0], x / y[1]];
};

var multiply = function(x, y) {
  if (Array.isArray(x) && Array.isArray(y)) return [x[0] * y[0], x[1] * y[1]];
  else if (Array.isArray(x)) return [x[0] * y, x[1] * y];
  else return [x * y[0], x * y[1]];
};

var negate = function(x) { return [-x[0], -x[1]]; };

var subtract = function(x, y) { return add(x, negate(y)); };
