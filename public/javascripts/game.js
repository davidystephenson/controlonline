var camera = Camera();

var canvas = $('#canvas')[0];

var context = canvas.getContext('2d');

var control = Control();

var client = false;

var game = {};

var locate = function(x) { return add(divide(subtract(x, camera.position), camera.scale), [500, 309]); };

var name = $('#name').text();

var print = function(obj) { return JSON.stringify(obj, null, 4); };

var socket = io();

var tick = function() {
  var actor;
  var key;
  context.clearRect(0, 0, 1000, 618);

  if (camera.track) {
    for (key in game.actors) {
      actor = game.actors[key];

      if (client.socketID && actor.client == client.socketID) {
        camera.position = actor.position;
      }
    }
  } else {
    camera.velocity = [0, 0];

    if (camera.up) camera.velocity[1] -= camera.speed;
    if (camera.down) camera.velocity[1] += camera.speed;
    if (camera.left) camera.velocity[0] -= camera.speed;
    if (camera.right) camera.velocity[0] += camera.speed;

    camera.position = add(camera.position, camera.velocity);
  }

  for (key in game.actors) {
    var destination;
    actor = game.actors[key];

    if (client.socketID && actor.client == client.socketID) {
      destination = locate(actor.mind.destination);

      context.fillStyle = 'red';
      context.beginPath();
      context.arc(destination[0], destination[1], 5 / camera.scale, 0, Math.PI*2, true); 
      context.closePath();
      context.fill();
    }
    else if (actor.type == 'player') context.fillStyle = 'blue';
    else if (actor.type == 'roamer') {
      var instability = locate(actor.instability);

      context.fillStyle = 'blue';
      context.beginPath();
      context.arc(instability[0], instability[1], 5 / camera.scale, 0, Math.PI*2, true); 
      context.closePath();
      context.fill();

      destination = locate(actor.mind.destination);
      context.fillStyle = 'green';

      context.beginPath();
      context.arc(destination[0], destination[1], 5 / camera.scale, 0, Math.PI*2, true); 
      context.closePath();
      context.fill();

    }

    else if (actor.type == 'wall') context.fillStyle = 'grey';

    for (key in actor.shapes) {
      var shape = actor.shapes[key];

      var position = locate(add(actor.position, shape.position));

      if (shape.type == 'circle') {
        context.beginPath();
        context.arc(position[0], position[1], shape.radius / camera.scale, 0, Math.PI * 2, true); 
        context.closePath();
        context.fill();
      } else if (shape.type == 'rectangle') {
        var topLeft = [
          position[0] - shape.width / camera.scale / 2, position[1] - shape.height / 2 / camera.scale
        ];
        context.fillRect(topLeft[0], topLeft[1], shape.width / camera.scale, shape.height / camera.scale);
      }
    }
  }

  $('#players').empty();

  for (key in game.clients) {
    var gameClient = game.clients[key];

    if (gameClient.type == 'player') {
      $('#players').append(
        '<a id="' + gameClient.name + '" class="list-group-item">' + gameClient.name + '</a>'
      );
    }

    if (gameClient.socketID == client.socketID) client = gameClient;
    if (client.name) $('#sign-on').hide();
  }

  $('#announcements').empty();

  for (key in game.announcements) {
    var announcement = game.announcements[key];

    if (announcement.client) if (announcement.client.socketID != client.socketID) continue;
    $('#announcements').prepend('<a class="list-group-item">' + announcement.text + '</a>');
  }

  socket.emit('control', control);
};

socket.emit('game', name);

$('#sign-off').hide();

socket.on('game', function(message) { game = message; });

socket.on('client', function(message) { client = message; });

$('#join').click(function() { control.name = $('#player').val(); });

setInterval(tick, 10);
